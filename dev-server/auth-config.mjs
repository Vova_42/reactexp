export const DEVELOPMENT_CONFIG = {
    AUTH_URL: '',
    AUTH_CLIENT_ID: '',
    AUTH_REDIRECT_URI: ''
};

export const TEST_CONFIG = {
    AUTH_URL: '',
    AUTH_CLIENT_ID: '',
    AUTH_REDIRECT_URI: ''
};

export const PRODUCTION_CONFIG = {
    AUTH_URL: '',
    AUTH_CLIENT_ID: '',
    AUTH_REDIRECT_URI: ''
};
